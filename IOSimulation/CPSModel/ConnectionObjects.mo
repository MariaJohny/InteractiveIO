within CPSModel;
package ConnectionObjects

  class SocketInput extends ExternalObject;

    function constructor
      input String socName = "_projectDir_/rpcSocketInput";
      output SocketInput connection;
      external "C" connection = initSocketConnection(socName)
        annotation(Library="socketInput",
          LibraryDirectory="modelica://CPSModel/ExternalLibraries");
    end constructor;


    function destructor "close connection"
      input SocketInput connection;
      external "C" closeSocketConnection(connection)
        annotation(Library="socketInput",
          LibraryDirectory="modelica://CPSModel/ExternalLibraries");
    end destructor;


  end SocketInput;

  class SocketOutput extends ExternalObject;

    function constructor
      input String socName = "_projectDir_/rpcSocketOutput";
      output SocketOutput connection;
      external "C" connection = initSocketConnection(socName)
        annotation(Library="socketOutput",
          LibraryDirectory="modelica://CPSModel/ExternalLibraries");
    end constructor;


    function destructor "close connection"
      input SocketOutput connection;
      external "C" closeSocketConnection(connection)
        annotation(Library="socketOutput",
          LibraryDirectory="modelica://CPSModel/ExternalLibraries");
   end destructor;


end SocketOutput;

end ConnectionObjects;
