within CPSModel;
package Functions

    function writeToSocket
      input CPSModel.ConnectionObjects.SocketOutput con;
      input Real userReply;
    
      external writeToSocket(con, userReply) annotation(
        Library = "socketOutput",
        LibraryDirectory = "modelica://CPSModel/ExternalLibraries");
    end writeToSocket;

    function readFromSocket
      input CPSModel.ConnectionObjects.SocketInput con;
      output Real userInput;
    
      external "C" userInput = readFromSocket(con) annotation(
        Library = "socketInput",
        LibraryDirectory = "modelica://CPSModel/ExternalLibraries");
    end readFromSocket;

end Functions;