within CPSModel;

package Models

block InputInterfaceBlock 

  CPSModel.ConnectionObjects.SocketInput con = CPSModel.ConnectionObjects.SocketInput("_projectDir_/rpcSocketInput");

  Modelica.Blocks.Interfaces.RealOutput userInput annotation(
    Placement(visible = true, transformation(origin = {-14, 8}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));

algorithm
    when sample(0.5,0.5) then
      print("Waiting for user input.\n");
      userInput := CPSModel.Functions.readFromSocket(con);
      print("Message user entered : " + String(userInput) + "\n");
    end when;
    
  annotation(
    Placement(visible = true, transformation(origin = {-70, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  annotation(
    __OpenModelica_simulationFlags(jacobian = "coloredNumerical", s = "dassl", lv = "LOG_STATS"),
    uses(Modelica(version = "3.2.2")),
    Icon(graphics = {Text(origin = {4, -1}, extent = {{-62, 73}, {62, -73}}, textString = "Input\nInterface", fontName = "DejaVu Sans Mono Bold")}));

end InputInterfaceBlock;

  block OutputInterfaceBlock
    CPSModel.ConnectionObjects.SocketOutput con = CPSModel.ConnectionObjects.SocketOutput("_projectDir_/rpcSocketOutput");
    
    Modelica.Blocks.Interfaces.RealInput outPut annotation(
      Placement(visible = true, transformation(origin = {2, 12}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  
  
  algorithm
      when outPut > 0 then
      print("Trying to send to socket : " + String(outPut + 1) + "\n");
      CPSModel.Functions.writeToSocket(con, outPut + 1);
      print("Message send to socket." + "\n");
      end when;
      
    annotation(
      Placement(visible = true, transformation(origin = {-70, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    annotation(
      __OpenModelica_simulationFlags(jacobian = "coloredNumerical", s = "dassl", lv = "LOG_STATS"),
      uses(Modelica(version = "3.2.2")),
      Icon(graphics = {Text(origin = {4, -1}, extent = {{-62, 73}, {62, -73}}, textString = "Output\nInterface", fontName = "DejaVu Sans Mono")}));
  
  end OutputInterfaceBlock;

  model MechatronicSystem
    CPSModel.Models.InputInterfaceBlock Input annotation(
      Placement(visible = true, transformation(origin = {-90, 8}, extent = {{-28, -28}, {28, 28}}, rotation = 0)));
  
    CPSModel.Models.OutputInterfaceBlock Output annotation(
      Placement(visible = true, transformation(origin = {72, -6}, extent = {{-28, -28}, {28, 28}}, rotation = 0)));
  equation
    connect(Input.userInput, Output.outPut) annotation(
      Line(points = {{-60, 8}, {100, 8}, {100, -6}, {102, -6}}, color = {0, 0, 127}));
    annotation(
      uses(Modelica(version = "3.2.2")));
  end MechatronicSystem;

end Models;
