#substitute in some the current directory for some hardcoded paths
projdir=$pwd
sed -i -e 's|_projectDir_|'$(pwd)'|g' socketInput.c socketOutput.c CPSModel/ConnectionObjects.mo CPSModel/Models.mo

#compilation of the external library
#clang -c -fPIC socketClient.c -o socketClient.o
#mkdir -p CPSModel/ExternalLibraries
#clang -shared socketClient.o -o CPSModel/ExternalLibraries/libsocketclient.so
#rm socketClient.o

#compilation of the external library
clang -c -fPIC socketInput.c -o socketInput.o
mkdir -p CPSModel/ExternalLibraries
clang -shared socketInput.o -o CPSModel/ExternalLibraries/libsocketInput.so
rm socketInput.o

#compilation of the external library
clang -c -fPIC socketOutput.c -o socketOutput.o
mkdir -p CPSModel/ExternalLibraries
clang -shared socketOutput.o -o CPSModel/ExternalLibraries/libsocketOutput.so
rm socketOutput.o
