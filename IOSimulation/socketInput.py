import json
import socket
import argparse
import os
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("-m", "--multiConn", action="store_true",
                    help="don't quit after first connection closes")

args = parser.parse_args()

s1 = socket.socket(socket.AF_UNIX)  

s1.bind('rpcSocketInput')
s1.listen(5)

#tells the socket library that we want it to queue up as many as 5 connect requests (the normal max) before refusing outside connections. If the rest of the code is written properly, that should be plenty. 

acceptFlag = True

while acceptFlag:
  s2,s2info = s1.accept()
  okay = True
  while okay:
    try:
      userInput = input('input something!: ')
      print "User input : %s " % userInput

      s2.sendall(np.array([userInput],dtype="double").tostring())
      print "Message send"

    except:
      okay = False
      s2.close()
  acceptFlag = args.multiConn


s1.close()
s2.close()
os.unlink('rpcSocketInput')
